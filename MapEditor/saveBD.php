<?php
	$connection = new PDO("oci:dbname=DECINFO", "e6118453", "c"); 

	$date 			  = $_POST["date"];
	$nomMap   	      = $_POST["nomMap"];
	$status 		  = $_POST["status"];
	$tailleMapRangee  = $_POST["tailleMapRangee"];
	$tailleMapColonne = $_POST["tailleMapColonne"];
	$tempsAnimMin 	  = $_POST["tempsAnimMin"];
	$tempsAnimMax 	  = $_POST["tempsAnimMax"];
	$mapModel 		  = $_POST["mapModel"];
	
	$statement = $connection->prepare("INSERT INTO TANKEMAPS(MAPDATE, MAPNAME, STATUS, MAPROW, MAPCOL, DELAYMIN, DELAYMAX, MAPMODEL) "."VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
	$statement->bindParam(1, $date);  
	$statement->bindParam(2, $nomMap);  
	$statement->bindParam(3, $status); 
	$statement->bindParam(4, $tailleMapRangee);  
	$statement->bindParam(5, $tailleMapColonne);  
	$statement->bindParam(6, $tempsAnimMin); 
	$statement->bindParam(7, $tempsAnimMax);	
	$statement->bindParam(8, $mapModel);
	$statement->execute(); 

	$connection = null;
?>