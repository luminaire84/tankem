
var mouseX, mouseY;

var imgPath;

var treeSelected;
var treeCompteur = 0;
var maxArbre = 4;

var selectedAsset = "";
var mapDate;
var peutEnregistrer = true;
var mapModel;

var joueur1Ajoute = false;
var joueur1Selectionne = false;
var joueur2Ajoute = false;
var joueur2Selectionne = false;

const MAP_TILE = {
	GRASS:			     "1",
	WALL:				 "2",
	WALL_ANIME:			 "3",
	GRASS_TREE:			 "4",
	WALL_TREE:			 "5",
	WALL_TREE_ANIME:	 "6",
	JOUEUR_1:			 "7",
	JOUEUR_2:			 "8",
	JOUEUR_1_GRASS:		 "9",
	JOUEUR_1_WALL:		 "10",
	JOUEUR_1_WALL_ANIME: "11",
	JOUEUR_1_GRASS:		 "12",
	JOUEUR_1_WALL:	 	 "13",
	JOUEUR_1_WALL_ANIME: "14",
}

$(function()
{
	$('#ajouterAssets').click(function(){
		$('#fileChooser').click();
	})

	$('body').on('click', '.mapTree', function(){
		if(!treeSelected && treeCompteur > 0){
			$(this).removeClass();
			treeCompteur--;		
		}
	})

	$('.asset').click(function(){
		$('.asset').css('box-shadow', 'none');
		$(this).css('box-shadow', '0 0 0 2px white');
		imgPath = $(this).css('background-image');

		selectedAsset =  $(this).attr('id');

		treeSelected = false;
		joueur1Selectionne = false;
		joueur2Selectionne = false;
	})

	$('.tree').click(function(){
		treeSelected = true;		
	})

	document.getElementById('joueur1').onclick = function(){
		joueur1Selectionne = true;
	}

	document.getElementById('joueur2').onclick = function(){
		joueur2Selectionne = true;
	}
	
	changerTailleMap();

	function enregistrerBD(){
		peutEnregistrer = true;
		joueur1Ajoute = false;
		joueur2Ajoute = false;
		mapModel = "";
		var table = document.getElementById("grille");
		for (var i = 0, row; row = table.rows[i]; i++) {		
			for (var j = 0, col; col = row.cells[j]; j++){	
				switch($(col).attr('id')) {		
					case "wall":
						mapModel += MAP_TILE.WALL;
						break;
					case "wallAnime":
						mapModel += MAP_TILE.WALL_ANIME;
						break;
					case "grassTree":
						mapModel += MAP_TILE.GRASS_TREE;
						break;
					case "wallTree":
						mapModel += MAP_TILE.WALL_TREE;
						break;
					case "wallTreeAnime":
						mapModel += MAP_TILE.WALL_TREE_ANIME;
						break;
					case "grass":
						mapModel += MAP_TILE.GRASS;
						break;
					case "joueur1":
						mapModel += MAP_TILE.JOUEUR_1;
						joueur1Ajoute = true;
						break;
					case "joueur2":
						mapModel += MAP_TILE.JOUEUR_2;
						joueur2Ajoute = true;
						break;
					default:
						peutEnregistrer = false;
				}	
			}  
			mapModel += "0";
		}

		if(peutEnregistrer)
		{
			if(joueur1Ajoute && joueur2Ajoute)
			{
				if( $('#mapName').val() != "")
				{
					if($('#min').val() != "" && $('#max').val() != "")
					{
						$('#SaveBox').show();			
						var date = mapDate;
						
						var nomMap = $('#mapName').val();
						
						var inputStatus = document.getElementById("status");
						var status = inputStatus.options[inputStatus.selectedIndex].value;

						var inputTailleMapRangee = document.getElementById("tailleMapRangee");
						var tailleMapRangee = inputTailleMapRangee.options[inputTailleMapRangee.selectedIndex].value;

						var inputTailleMapCol = document.getElementById("tailleMapColonne");
						var tailleMapColonne = inputTailleMapCol.options[inputTailleMapCol.selectedIndex].value;

						var tempsAnimMin = $('#min').val();
						var tempsAnimMax = $('#max').val();

						$.ajax({
							url: "uploadBD.php",
							type: "POST",
							data: {
								date,
								nomMap,
								status,
								tailleMapRangee,
								tailleMapColonne,
								tempsAnimMin,
								tempsAnimMax,
								mapModel
							}
						})
						.done(function (donnees){
							console.log('***** SUCCESFULLY SAVED TO DB *****');
						})
					}
					else
						$('#minMaxVide').show();		
				}
				else
					$('#nomVide').show();
			}
			else
				$('#playerBox').show();
		}
		else
			$('#cantSaveBox').show();
	}

	$('#save').click(function(){
		enregistrerBD();
	})
})

var oldColonne = null;
var oldRangee;
var colonne;
var rangee;
function changerTailleMap(){
	treeCompteur = 0;
	
	var table = document.getElementById('grille');
	colonne  = document.getElementById('tailleMapColonne').value;
	rangee   = document.getElementById('tailleMapRangee').value;

	table.innerHTML = "";

	for(var i = 0; i < rangee; ++i){
		var tr = table.insertRow();

		for(var j = 0; j < colonne; ++j){
			var td = tr.insertCell();

			td.style.border = '1px solid black';
			td.style.backgroundImage = "url('assets/grass.png')";
			td.style.backgroundPosition = "center";
			td.style.backgroundSize = "cover";			
			$(td).attr('id', "grass");
		}
	}

	$('td').click(function(){	
		if($(this).attr('id') == "joueur1"){
			joueur1Ajoute = false;
		}
		else if($(this).attr('id') == "joueur2"){
			joueur2Ajoute = false;
		}

		if(treeSelected && treeCompteur < maxArbre && !$(this).hasClass('mapTree')){
			if(treeCompteur < 4){
				addAssestToMap(this);
				$(this).addClass('mapTree');	
				treeCompteur++;
			}				
		}

		else if(joueur1Selectionne || joueur2Selectionne){
			if(joueur1Selectionne && !joueur1Ajoute){
				$(this).attr('id', selectedAsset);	
				$(this).css('backgroundImage', imgPath + ", " + $(this).css('backgroundImage'));
				$(this).css('backgroundRepeat', 'no-repeat');
				$(this).css('backgroundSize', 'cover');
				$(this).css('backgroundPosition', 'center, center');
				joueur1Ajoute = true;
			}
			else if(joueur2Selectionne && !joueur2Ajoute){
				$(this).attr('id', selectedAsset);
				$(this).css('backgroundImage', imgPath + ", " + $(this).css('backgroundImage'));
				$(this).css('backgroundRepeat', 'no-repeat');
				$(this).css('backgroundSize', 'cover');
				$(this).css('backgroundPosition', 'center, center');
				joueur2Ajoute = true;		
			}
		}

		else if(!treeSelected){
			addAssestToMap(this);
		}

		function addAssestToMap(node){
			$(node).attr('id', selectedAsset);
			$(node).css('background', imgPath);
			$(node).css('backgroundSize', 'cover');
			$(node).css('backgroundPosition', 'center');
		}
	})
}
