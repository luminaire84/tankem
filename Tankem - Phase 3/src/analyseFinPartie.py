# -*- coding: utf-8 -*-

from gameLogic 	 	import *
from dtoStatistique import *

class AnalyseFinPartie():

	def __init__(self):
		self.dtoStatistique = DTOStatistique()
		
	#Calculer l'experience du gagnant
	def expGagnant(self, siFavori, vieRestant, nomCalcule, dtoGagnant, nbArmesUtilisees):

		self.expOwnGagnant 		= dtoGagnant[5]
		self.niveauCourrant 	= dtoGagnant[2]
		self.bonusExpGagnant 	= 100 + 100 * siFavori + vieRestant * 2
		self.seuilNextLvlWinner = 100*(self.niveauCourrant+1)+50*(self.niveauCourrant**2)
		self.expTotalGagnant 	= self.expOwnGagnant + self.bonusExpGagnant
		self.lvlUp 				= False

		if self.expTotalGagnant >= self.seuilNextLvlWinner:
			self.lvlUp = True
			self.niveauCourrant += 1
			self.seuilNextLvlWinner = 100*(self.niveauCourrant+1)+50*(self.niveauCourrant**2)
			self.dtoStatistique.joueur1["pointsStats"] = dtoGagnant[6] + 5
		else:
			self.dtoStatistique.joueur1["pointsStats"] = dtoGagnant[6]

		self.dtoStatistique.joueur1["bonusExp"] 	   = self.bonusExpGagnant	
		self.dtoStatistique.joueur1["expTotal"] 	   = self.expTotalGagnant
		self.dtoStatistique.joueur1["niveau"] 		   = self.niveauCourrant
		self.dtoStatistique.joueur1["seuilNextLevel"]  = self.seuilNextLvlWinner
		self.dtoStatistique.joueur1["lvlUp"] 		   = self.lvlUp
		self.dtoStatistique.joueur1["idUser"] 		   = dtoGagnant[1]	
		self.dtoStatistique.joueur1["nomCalcule"] 	   = nomCalcule
		self.dtoStatistique.joueur1["resultat"] 	   = "v"			
		self.dtoStatistique.joueur1["nbArmesUtilisees"] = nbArmesUtilisees			
					
		return self.dtoStatistique.joueur1

	#Calculer l'experience du perdant
	def expPerdant(self, vieGagnantPerdue, nomCalcule, dtoPerdant, nbArmesUtilisees):

		self.expOwnPerdant 	   	= dtoPerdant[5]
		self.niveauCourrant 	= dtoPerdant[2]
		self.bonusExpPerdant   	= vieGagnantPerdue * 2
		self.seuilNextLvlLoser 	= 100*(self.niveauCourrant+1)+50*(self.niveauCourrant*self.niveauCourrant)
		self.expTotalPerdant   	= self.expOwnPerdant + self.bonusExpPerdant
		self.lvlUp 				= False

		if self.expTotalPerdant >= self.seuilNextLvlLoser:
			self.lvlUp = True
			self.niveauCourrant += 1
			self.seuilNextLvlLoser 	= 100*(self.niveauCourrant+1)+50*(self.niveauCourrant*self.niveauCourrant)
			self.dtoStatistique.joueur2["pointsStats"] = dtoPerdant[6] + 5
		else:
			self.dtoStatistique.joueur2["pointsStats"] = dtoPerdant[6]

		self.dtoStatistique.joueur2["bonusExp"] 	   = self.bonusExpPerdant			
		self.dtoStatistique.joueur2["expTotal"] 	   = self.expTotalPerdant
		self.dtoStatistique.joueur2["niveau"] 		   = self.niveauCourrant
		self.dtoStatistique.joueur2["seuilNextLevel"]  = self.seuilNextLvlLoser
		self.dtoStatistique.joueur2["lvlUp"] 		   = self.lvlUp
		self.dtoStatistique.joueur2["idUser"] 		   = dtoPerdant[1]
		self.dtoStatistique.joueur2["nomCalcule"] 	   = nomCalcule	
		self.dtoStatistique.joueur2["resultat"] 	   = "d"		
		self.dtoStatistique.joueur2["nbArmesUtilisees"] = nbArmesUtilisees			
			
				
		return self.dtoStatistique.joueur2