# -*- coding: utf-8 -*-

class AnalyseDTOJoueur():
	def __init__(self):	
		self.listQualificatifA  = ["le fougeux", "le pétulant", "l'immortel", "le crossfiter", "le hulk", "le tout puissant", 
									"le prompt", "le lynx", "le foudroyant", "le précis", "l'habile", "le chirurgien"]
		self.listQualificatifB  = ["fougeux", "pétulant", "immortel", "qui fait du crossfit", "brutal", "tout puissant", 
									"prompt", "lynx", "foudroyant", "précis", "habile", "chirurgien"]
		
	
	def nomCalcule(self, dtoJoueur):
		self.attributVie 	   = dtoJoueur[0][7]
		self.attributForce 	   = dtoJoueur[0][9]
		self.attributAgilite   = dtoJoueur[0][8]
		self.attributDexterite = dtoJoueur[0][10]
		self.listCriteres 	   = [self.attributVie, self.attributForce, self.attributAgilite, self.attributDexterite]

		nomCal    	  = ""
		nomJoueur 	  = dtoJoueur[2]
		critereA      = 0
		critereB  	  = 0	
		qualificatifA = ""
		qualificatifB = ""
		n = 0
		if self.attributVie == 30 and self.attributForce == 30 and self.attributAgilite == 30 and self.attributDexterite == 30:
			qualificatifA = "dominateur"
		else:
			for i in self.listCriteres:
				if critereA < i:
					critereA = i
					if i >= 1 and i < 5:
						qualificatifA = self.listQualificatifA[n*3]
					elif i >= 5 and i < 10:
						qualificatifA = self.listQualificatifA[n*3+1]
					else:
						qualificatifA = self.listQualificatifA[n*3+2]
				n += 1

			n = 0
			for i in self.listCriteres:
				if critereB < i and i < critereA:		# ou if critereB <= i and i < critereA (les 2 conditions au choix)
					critereB = i
					if i >= 1 and i < 5:
						qualificatifB = self.listQualificatifB[n*3]
					elif i >= 5 and i < 10:
						qualificatifB = self.listQualificatifB[n*3+1]
					else:
						qualificatifB = self.listQualificatifB[n*3+2]
				n += 1

		nomCal = nomJoueur+" "+qualificatifA+" "+qualificatifB
		return nomCal
