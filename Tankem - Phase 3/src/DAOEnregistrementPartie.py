# -*- coding: utf-8 -*-

import cx_Oracle
from   DTOEnregistrementPartie import *
from   connectionOracleSingleton import *

class DAOEnregistrementPartie():

	def __init__(self):

		#self.dto_enregistrement = DTOEnregistrementPartie()
		#self.ecrireDansBD()
		pass


	def ecrireDansBD(self,DTOEnregistrementPartie):

		self.connection = ConnectionOracleSingleton().getConnectionOracle()

		cursor    = self.connection.cursor()
		statement ='SELECT max(id_partie) FROM parties' #On récupère la dernière l'id de la partie (la dernière)
		cursor.execute(statement)
		resultat = cursor.fetchall()
		idPartie = resultat[0][0] + 1
		cursor.close()

		#Ajoute le ID de la partie pour chaque données
		for i in range(len(DTOEnregistrementPartie.infoTank1)):
			DTOEnregistrementPartie.infoTank1[i].append(idPartie)

		for i in range(len(DTOEnregistrementPartie.infoTank2)):
			DTOEnregistrementPartie.infoTank2[i].append(idPartie)			

		for i in range(len(DTOEnregistrementPartie.lesBalles)):
			DTOEnregistrementPartie.lesBalles[i].append(idPartie)
			
		for i in range(len(DTOEnregistrementPartie.lesItems)):
			DTOEnregistrementPartie.lesItems[i].append(idPartie)

		print "***********************--------------------------------****************************22222222222222222222222222222"

		sqlBalle = "INSERT INTO BALLE(POSITIONX,POSITIONY,ETAT,IDENTIFIANT_LANCEUR, ID_PARTIE) VALUES (:1,:2,:3,:4,:5)"
		sqlItem  = "INSERT INTO ITEMS(POSITIONX,POSITIONY,TYPE_ITEM, ID_PARTIE) VALUES (:1,:2,:3,:4)"
		sqlTank  = "INSERT INTO LES_TANKS(ID_TANK,POSITIONX,POSITIONY,ORIENTATION,POINT_VIE, ID_PARTIE) VALUES (:1,:2,:3,:4,:5,:6)"

		cur = self.connection.cursor()
		cur.bindarraysize = 5
		cur.setinputsizes(int, int, int, int, int)
		cur.executemany(sqlTank, DTOEnregistrementPartie.infoTank1)
		cur.close()

		cur2 = self.connection.cursor()
		cur2.bindarraysize = 5
		cur2.setinputsizes(int, int, int, int, int)
		cur2.executemany(sqlTank, DTOEnregistrementPartie.infoTank2)
		cur2.close()
		
		cur3 = self.connection.cursor()
		cur3.bindarraysize = 4
		cur3.setinputsizes(int, int, str, int)
		cur3.executemany(sqlBalle, DTOEnregistrementPartie.lesBalles)
		cur3.close()
		
		cur4 = self.connection.cursor()
		cur4.bindarraysize = 3
		cur4.setinputsizes(int, int, str)
		cur4.executemany(sqlItem, DTOEnregistrementPartie.lesItems)
		cur4.close()
		
		self.connection.commit()