# -*- coding: utf-8 -*-

from 	direct.gui.OnscreenText 		import OnscreenText
from 	direct.gui.OnscreenImage 		import OnscreenImage
from 	direct.gui.DirectGui 			import * 
from 	panda3d.core 					import TextNode 
from 	panda3d.core 					import Point3
from 	panda3d.bullet 					import BulletWorld
from 	direct.interval.IntervalGlobal 	import *
from 	direct.interval.LerpInterval 	import *

import 	sys
import 	time
import 	webbrowser
from 	daoUser 						import *
from 	entity 							import *
from    analyseDTOJoueur 				import *

class Menu():

	def __init__(self, startGame, dtoNiveau):
		self.analyseDTOJoueur   = AnalyseDTOJoueur()
		self.daoUser			= DAOUser()
		self.startGame  		= startGame
		self.dtoNiveau  		= dtoNiveau;
		self.titreMap   		= None
		self.frameMenuPrincipal = None
		self.frameMenuNiveaux	= None
		self.frameLogin	     	= None
		self.frameCombat	    = None		
		self.nomsMap 			= []
		self.compteur			= 0;
		self.J1Logged 			= False
		self.J2Logged 			= False
		self.fontBlackOps = loader.loadFont('fonts/BlackOpsOne-Regular.ttf')
		self.menuPrincipal()

	#--------------------------------------------------------------------------------
	#-------------------------------- MENU PRINCIPAL --------------------------------

	def menuPrincipal(self):

		if self.frameMenuNiveaux is not None:

			self.frameMenuNiveaux.destroy()

		self.frameMenuPrincipal = DirectFrame(image = "assets/tankemMenuBG.jpg", image_scale=(2,1,1))
		
		OnscreenText(text = "Tankem", pos = (0.0,0.5), scale = 0.3, fg=(0,0,0,0.8),align=TextNode.ACenter,mayChange=0, parent=self.frameMenuPrincipal, font = self.fontBlackOps)
		self.btnNiveaux = DirectButton(text = ("Visualiser les cartes"), pos = (0.0,0.0,-0.6), scale =0.1, command = self.menuNiveaux, pad=(0.25,0.25), parent=self.frameMenuPrincipal)
		self.btnQuitter = DirectButton(text = ("Quitter"), pos = (0.0,0.0,-0.8), text_scale=(1,1), scale =0.1, command = sys.exit, pad=(0.25,0.25), parent=self.frameMenuPrincipal)

	#------------------------------------------------------------------------------
	#-------------------------------- MENU NIVEAUX --------------------------------

	def menuNiveaux(self):

		if self.frameMenuNiveaux is not None:
			self.daoUser.tempEmail = ""
			self.btnRetour.destroy()
			self.frameMenuNiveaux.destroy()	

		self.btnNiveaux.destroy()
		self.btnQuitter.destroy()

		self.frameMenuNiveaux 	= DirectFrame(image = "assets/bg.jpg", image_scale=(2,1,1), pos=(0, 0, 0))
		
		frameUp 			 	= LerpPosInterval(self.frameMenuNiveaux, 0.5, Vec3(0, 0, 0), Vec3(0, 0, -5))	
		frameUp.start()

		self.nomsMap = [] #Contient le nom de toutes les maps
		self.nomsMap.append("HASARD")

		for i in self.dtoNiveau.listeMaps:	
			self.nomsMap.append(i)
		
		self.compteur  = 0 #Pour naviguer entre les différentes maps

		self.titreMap  = OnscreenText(text=self.nomsMap[self.compteur],pos=(0.0,0),scale = 0.22,fg=(250,250,250,0.8),align=TextNode.ACenter,mayChange=0,parent=self.frameMenuNiveaux,font=self.fontBlackOps)		
		btnNext 	   = DirectButton(text="Next",pos=(0.27,0.0,-0.57),scale=0.08,command=self.changerNiveauSuivant,pad=(1.8,0.65),parent=self.frameMenuNiveaux)
		btnPrevious    = DirectButton(text="Previous",pos=(-0.27,0.0,-0.57),scale=0.08,command=self.changerNiveauPrecedent,pad=(0.55,0.65),parent=self.frameMenuNiveaux)
		self.btnLaunch = DirectButton(text="Launch game",pos=(0.0,0.0,-0.85),scale=0.08,command=self.menuLogin,pad=(0.55,0.65),parent=self.frameMenuNiveaux)
		self.btnRetour = DirectButton(text="Back to main menu",pos=(-1.2,0.0,0.8),scale=0.08,command=self.menuPrincipal,pad=(0.55,0.65),parent=self.frameMenuNiveaux)

	def changerNiveauSuivant(self):

		self.compteur += 1

		if self.compteur >= len(self.nomsMap):
			self.compteur = len(self.nomsMap) - 1

		self.titreMap.destroy()
		self.titreMap = OnscreenText(text = self.nomsMap[self.compteur], pos = (0.0,0), scale = 0.22, fg=(250,250,250,0.8),align=TextNode.ACenter,mayChange=0, parent=self.frameMenuNiveaux, font=self.fontBlackOps)

	def changerNiveauPrecedent(self):

		self.compteur -= 1

		if self.compteur < 0:
			self.compteur = 0

		self.titreMap.destroy()
		self.titreMap = OnscreenText(text = self.nomsMap[self.compteur], pos = (0.0,0), scale = 0.22, fg=(250,250,250,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameMenuNiveaux, font=self.fontBlackOps)

	#----------------------------------------------------------------------------
	#-------------------------------- MENU LOGIN --------------------------------

	def menuLogin(self):

		self.btnRetour.destroy()

		if self.frameLogin is not None:

			self.frameLogin.destroy()

		self.btnLaunch.destroy()
		self.frameLogin = DirectFrame(image = "assets/beforeBattleBG.jpg", image_scale=(2,1,1))

		self.btnRetour = DirectButton(text="Back",pos=(-1.42,0.0,0.8),scale=0.08,command=self.menuNiveaux,pad=(0.55,0.65),parent=self.frameLogin)

		#Animation du menu translation de bas en haut
		frameUp = LerpPosInterval(self.frameLogin, 0.5, Vec3(0, 0, 0), Vec3(0, 0, -5))	
		frameUp.start()

		self.titre = OnscreenText(text = "Login Joueur 1", pos = (-1,0.5), scale = 0.15, fg=(250,250,250,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
		self.titre = OnscreenText(text = "Login Joueur 2", pos = (0.9,0.5), scale = 0.15, fg=(250,250,250,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
				
		OnscreenText(text = "Email", pos = (-1.27,0.25), scale = .07, fg=(250,250,250,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
		self.usernameJ1   = DirectEntry(text = "", pos = (-1.1,0,0.25), scale=.07,command="", numLines = 1,focus=0, parent=self.frameLogin)
		OnscreenText(text = "Password", pos = (-1.35,0.1), scale = .07, fg=(250,250,250,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
		self.passwordJ1   = DirectEntry(text = "", pos = (-1.1,0,0.1),scale=.07,command="", numLines = 1,focus=0,obscured=1, parent=self.frameLogin)
		self.btnJ1 		  = DirectButton(text = "Login", pos = (-0.95,0.0,-0.15), scale = 0.08, command = self.loginPlayer1, pad=(0.9,0.35), parent=self.frameLogin)
		self.messageJ1 	  = OnscreenText(text = "En attente...", pos = (-0.92,-0.35), scale = .07, fg=(250,0,0,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)		
		
		OnscreenText(text = "Email", pos = (0.47,0.25), scale = .07, fg=(250,250,250,0.8),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
		self.usernameJ2   = DirectEntry(text = "", pos = (0.66,0,0.25), scale=.07,command="", numLines = 1,focus=0, parent=self.frameLogin)		
		OnscreenText(text = "Password", pos = (0.4,0.1), scale = .07, fg=(250,250,250,0.8),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
		self.passwordJ2   = DirectEntry(text = "", pos = (0.66,0,0.1),scale=.07,command="", numLines = 1,focus=0,obscured=1, parent=self.frameLogin)
		self.btnJ2 		  = DirectButton(text = "Login", pos = (0.85,0.0,-0.15), scale = 0.08, command = self.loginPlayer2, pad=(0.9,0.35), parent=self.frameLogin)
		self.messageJ2    = OnscreenText(text = "En attente...", pos = (0.9,-0.35), scale = .07, fg=(250,0,0,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
	
	def loginPlayer1(self):

		if self.daoUser.loginUser(1, self.usernameJ1.get(), self.passwordJ1.get()): #Afficher le message connected si le joueur 1 s'est bien authentifié et le bouton est désactivé
			self.messageJ1.destroy()

			self.messageJ1 		= OnscreenText(text = "Connected", pos = (-0.92,-0.35), scale = .07, fg=(0,250,0,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
			self.btnJ1['state'] = DGG.DISABLED
			self.J1Logged 		= True

			self.menuCombat()

	def loginPlayer2(self):

		if self.daoUser.loginUser(2, self.usernameJ2.get(), self.passwordJ2.get()):#Afficher le message connected si le joueur 2 s'est bien authentifié et le bouton est désactivé
			self.messageJ2.destroy()

			self.messageJ2 		= OnscreenText(text = "Connected", pos = (0.9,-0.35), scale = .07, fg=(0,250,0,0.9),align=TextNode.ACenter,mayChange=0, parent=self.frameLogin, font=self.fontBlackOps)
			self.btnJ2['state'] = DGG.DISABLED
			self.J2Logged 		= True

			self.menuCombat()

	#-------------------------------------------------------------------------------
	#--------------------------------- MENU COMBAT ---------------------------------
	
	def menuCombat(self):

		self.frameMenuPrincipal.destroy()
		self.frameMenuNiveaux.destroy()

		#Si les deux joueurs sont connecté
		if self.J1Logged and self.J2Logged: 

			self.frameLogin.destroy()

			joueur1 = self.daoUser.dtoUsers.joueur1
			joueur2 = self.daoUser.dtoUsers.joueur2
			
			joueurFavorise   = self.calcJoueurFavoris(joueur1, joueur2) #Calcule le joueur favorisé
			
			self.frameCombat = DirectFrame()		
			# self.nomJoueur1	 = OnscreenText(text=joueur1[0][4], pos=(-0.4,0.5), scale = 0.09, fg=(250,250,250,0.9),align=TextNode.ARight,mayChange=0, parent=self.frameCombat, font=self.fontBlackOps)
			self.nomJoueur1	 = OnscreenText(text=self.analyseDTOJoueur.nomCalcule(joueur1), pos=(-0.3,0.5), scale = 0.06, fg=(250,250,250,0.9),align=TextNode.ARight,mayChange=0, parent=self.frameCombat, font=self.fontBlackOps)
			
			# self.nomJoueur2  = OnscreenText(text=joueur2[0][4], pos=(0.4,0.5), scale = 0.09, fg=(250,250,250,0.9),align=TextNode.ALeft,mayChange=0, parent=self.frameCombat, font=self.fontBlackOps)
			self.nomJoueur2  = OnscreenText(text=self.analyseDTOJoueur.nomCalcule(joueur2), pos=(0.3,0.5), scale = 0.06, fg=(250,250,250,0.9),align=TextNode.ALeft,mayChange=0, parent=self.frameCombat, font=self.fontBlackOps)
			
			self.vsImage     = OnscreenImage(pos=(0, 0, 0.5), image = "assets/vs.png", scale=(0.2,0.1,0.1), parent=self.frameCombat)
			self.vsImage.setTransparency(TransparencyAttrib.MAlpha)
			OnscreenText(text="Combattrons dans " + self.nomsMap[self.compteur], pos=(0,0.25), scale = 0.07, fg=(250,250,250,1),align=TextNode.ACenter,mayChange=0, parent=self.frameCombat, font=self.fontBlackOps)

			#Le nom du joueur sera affiché s'il est favorisé
			if joueurFavorise != "":
				OnscreenText(text=joueurFavorise + " est favorisé !", pos=(0,0.17), scale = 0.06, fg=(250,250,250,1),align=TextNode.ACenter,mayChange=0, parent=self.frameCombat, font=self.fontBlackOps)
			
			self.btnCombattre = DirectButton(text = "Combattre !", pos = (0,0.0,-0.8), scale = 0.1, command = self.tankFaceToFaceAnim, pad=(0.35,0.35), parent=self.frameCombat)
			self.btnPageWeb   = DirectButton(text="Page web", pos = (1.3,0.0,-0.8), scale = 0.08, command =self.webPage, pad=(0.25,0.25), parent=self.frameCombat)

			#Lerp bouton Combat
			intervalUp    = LerpScaleInterval(self.btnCombattre, 0.8, 0.1, 0.13)
			intervalDown  = LerpScaleInterval(self.btnCombattre, 0.8, 0.13, 0.1)			
			self.btnScale = Sequence(intervalUp,intervalDown)
			self.btnScale.loop()
				
			self.tank1 = loader.loadModel("../asset/Tank/tank")
			self.tank1.setColorScale(Vec4(float(joueur1[1][0])/255, float(joueur1[1][1])/255, float(joueur1[1][2])/255,1))		
			self.tank1.setScale(Vec3(2.8,2.8,2.8))
			self.tank1.setHpr(Vec3(-90,0,0))
			self.tank1.reparentTo(render)		

			#Lerp -Rotation- du Tank1
			self.animationTank1 = None
			self.isRotate = False #Pour lancer la partie seulement lorsque les Tanks commencent à Tourner 

			def rotateTank1():	

				self.isRotate = True
				self.animationTank1.finish()
				tank1Rotation = LerpHprInterval(self.tank1, 4, Vec3(-450,0,0), Vec3(-90,0,0))	
				self.animationTank1 = Sequence(tank1Rotation)
				self.animationTank1.loop()
			
			tank1Translation = LerpPosInterval(self.tank1, 3, Vec3(-13,-9,2), Vec3(-25.5,-9,2))
			self.animationTank1 = Sequence(tank1Translation, Func(rotateTank1))
			self.animationTank1.loop()

			self.tank2 = loader.loadModel("../asset/Tank/tank")
			self.tank2.setColorScale( Vec4(float(joueur2[1][0])/255, float(joueur2[1][1])/255, float(joueur2[1][2])/255,1) )
			self.tank2.setScale(Vec3(2.8,2.8,2.8))
			self.tank2.setHpr(Vec3(90,0,0))
			self.tank2.reparentTo(render)

			#Lerp -Rotation- du Tank2
			self.animationTank2 = None

			def rotateTank2():

				self.animationTank2.finish()
				tank1Rotation = LerpHprInterval(self.tank2, 4, Vec3(450,0,0), Vec3(90,0,0))	
				self.animationTank2 = Sequence(tank1Rotation)
				self.animationTank2.loop()

			tank2Translation = LerpPosInterval(self.tank2, 3, Vec3(13,-9,2), Vec3(25.5,-9,2))
			self.animationTank2 = Sequence(tank2Translation, Func(rotateTank2) )
			self.animationTank2.loop()
		
	#Animation pour que les 2 tanks se font face
	def tankFaceToFaceAnim(self):

		if self.isRotate:
			self.btnCombattre.destroy()

			self.animationTank1.finish()
			tank1Rotation = LerpHprInterval(self.tank1, 2, Vec3(-90,0,0), self.tank1.getHpr())	
			tank1Rotation.start()

			self.animationTank2.finish()
			tank2Rotation = LerpHprInterval(self.tank2, 2, Vec3(90,0,0), self.tank2.getHpr())	
			self.animationTank2 = Sequence(tank2Rotation, Func(self.vsAnim)) #Appel l'animation du "VS", arrête les lerps des 2 Tanks
			self.animationTank2.loop()

	#Animation avant que le combat commence - Grossissement et rapetissement -
	def vsAnim(self):

		self.animationTank2.finish()
		self.btnCombattre.destroy()
		self.btnPageWeb.destroy()

		intervalUp 		= LerpScaleInterval(self.vsImage, 1, 0.35, 0.2)
		intervalDown 	= LerpScaleInterval(self.vsImage, 1, 0.2, 0.35)			
		self.vsImageSeq = Sequence(intervalUp,intervalDown,intervalUp,intervalDown)
		self.vsImageSeq.loop()

		intervalUp 		   = LerpScaleInterval(self.nomJoueur1, 1, 1.2, 0.8)
		intervalDown 	   = LerpScaleInterval(self.nomJoueur1, 1, 0.8, 1.2)			
		self.nomJoueur1Seq = Sequence(intervalUp,intervalDown,intervalUp,intervalDown)
		self.nomJoueur1Seq.loop()

		intervalUp 	       = LerpScaleInterval(self.nomJoueur2, 1, 1.2, 0.8)
		intervalDown       = LerpScaleInterval(self.nomJoueur2, 1, 0.8, 1.2)			
		self.nomJoueur2Seq = Sequence(intervalUp,intervalDown,intervalUp,intervalDown,Func(self.fadeOut))
		self.nomJoueur2Seq.loop()

	def fadeOut(self):

		self.nomJoueur1Seq.finish()
		self.nomJoueur2Seq.finish()
		self.circle = OnscreenImage(pos=(0, 0, 0), image = "assets/circle.png", scale=(0,0,0))
		self.circle.setTransparency(TransparencyAttrib.MAlpha)
		scalelUp = LerpScaleInterval(self.circle, 1.2, 4, -0.1)		
		self.scalelUpSeq = Sequence(scalelUp,Func(self.launchGame))
		self.scalelUpSeq.loop()

	def launchGame(self):	

		self.scalelUpSeq.finish()
		self.circle.destroy()

		self.tank1.detachNode()		
		self.tank1.removeNode()
		self.tank2.detachNode()		
		self.tank2.removeNode()

		self.frameCombat.destroy()
		self.startGame(self.nomsMap[self.compteur], self.daoUser.dtoUsers) #Lance le jeu avec la map HASARD ou la map choisie

	def calcJoueurFavoris(self, joueur1, joueur2):

		joueurFavorise = ""

		#Compare les niveaux des Joueurs
		if joueur1[0][2] > joueur2[0][2]: 
			joueurFavorise = self.analyseDTOJoueur.nomCalcule(joueur1)
		if joueur2[0][2] > joueur1[0][2]:
			joueurFavorise = self.analyseDTOJoueur.nomCalcule(joueur2)
		
		return joueurFavorise

	def webPage(self):
		url="http://localhost/Web/"
		webbrowser.get('C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s').open(url)
