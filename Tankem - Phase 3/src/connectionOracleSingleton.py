#-*- coding: utf-8 -*-

import cx_Oracle

class ConnectionOracleSingleton():
    __shared_connection = {}

    def __init__(self):

        #Le dictionnaire d'attibuts sera le meme pour toutes les instances
        self.__dict__ = self.__shared_connection

        #Attention il ne faut pas ecraser les attributs existant lorsqu'on repasse constament dans ce code
        if not hasattr(self,"connection"):
            self.connection = self.connectOracle()

    def connectOracle(self):
        connection = None

        try:
            connection 	= cx_Oracle.connect('e1464900', "B",'10.57.4.60/DECINFO.edu')

        except cx_Oracle.DatabaseError as e:
            print("Problème de lecture de la base de données")

        return connection

    def disconnectOracle(self):
         self.connection.close()

    def getConnectionOracle(self):
        return self.connection