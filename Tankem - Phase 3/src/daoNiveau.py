# -*- coding: utf-8 -*-

import cx_Oracle
from   connectionOracleSingleton import *
from   dtoNiveau import *
import ctypes

class DAONiveau():
	def __init__(self):
		self.resultMaps    = None
		self.resultJoueurs = None
		self.LireBD()

	def LireBD(self):
		connection = ConnectionOracleSingleton().getConnectionOracle()

		if connection is not None:
			curRead    = connection.cursor()

			curRead.execute("select * from TANKEMAPS ORDER BY MAPNAME")
			self.resultMaps = curRead.fetchall()

			curRead.execute("select * from JOUEUR")
			self.resultJoueurs = curRead.fetchall()

			curRead.close()
			
	def construireDTO(self):
		dtoNiveau = DTONiveau()
		connection = ConnectionOracleSingleton().getConnectionOracle()
		
		if connection is not None:
			curRead    = connection.cursor()

			if(self.resultJoueurs is not None):	
				for i in self.resultJoueurs:		
					idMap = i[0]	
					sql = "SELECT MAPNAME from TANKEMAPS WHERE ID = :id"			
					curRead.execute(sql, id=i[0])
					nomMap = curRead.fetchall()

					dtoNiveau.listeJoueurs.append([nomMap[0][0], i])
			


			if(self.resultMaps is not None):
				for i in self.resultMaps:
					dtoNiveau.listeMaps[i[1]] = i

					sql = "SELECT * from MAPSTRUCTURE WHERE ID = :id"
						
					curRead.execute(sql, id=i[0])
					dtoNiveau.listeStructures[i[1]] = curRead.fetchall()

			curRead.close()

		return dtoNiveau


