# -*- coding: utf-8 -*-

from dtoBalance 			   import *
from dtoUsers 				   import *
from connectionOracleSingleton import *
import cx_Oracle
import ctypes
import bcrypt

class DAOBalanceOracle():

	def __init__(self):

		self.connection     = None
		self.resultStats    = None
		self.resultMessages = None
		self.LireBD()

	def LireBD(self):
		self.connection = ConnectionOracleSingleton().getConnectionOracle()

		if self.connection is not None:
			curRead = self.connection.cursor()

			curRead.execute("select * from INFO_JEU")
			self.resultStats = curRead.fetchall() #Creer un tableau à partir du curseur

			curRead.execute("select * from MESSAGE")
			self.resultMessages = curRead.fetchall() #Creer un tableau à partir du curseur

			curRead.close()

		else:
			self.afficherErreur("Impossible de se connecter a la BD", "ERREUR")

	def construireDTO(self):

		dtoBalance = DTOBalance()

		if(self.resultStats is not None and self.resultMessages is not None):

			dtoBalance.balanceActuelle.clear()
			
			for i in self.resultStats:

				dtoBalance.balanceMin[i[1]] 	 = i[2]
				dtoBalance.balanceMax[i[1]] 	 = i[3]
				dtoBalance.balanceDefaut[i[1]]   = i[4]
				dtoBalance.balanceActuelle[i[1]] = i[5]

			for i in self.resultMessages:

				dtoBalance.balanceMin[i[1]] 	 = i[2]
				dtoBalance.balanceMax[i[1]] 	 = i[3]
				dtoBalance.balanceDefaut[i[1]]   = i[4]

		return dtoBalance

	def afficherErreur(self, message, titre):

		ctypes.windll.user32.MessageBoxA(0, str(message), titre, 0)

	def ecrireBaseDonnee(self, balance):

		valide		 = True
		valeurErrone = ""

		try:
			self.connection = ConnectionOracleSingleton().getConnectionOracle();

			cur = self.connection.cursor()

			for i in balance:

				if "Message" not in i[0]: #Si la balance est un stats
					sql = "UPDATE INFO_JEU set CURRENTVALUE = :1 WHERE DESCRIPTION = :2"

					#Si la valeur n'est pas un nombre et n'est pas vide
					try:
						if(float(i[4]) is str and i[4] is not ""):
							self.afficherErreur("Valeur actuelle n'est pas un nombre", "ERREUR")
							valide = False
							break
					except ValueError:
						if(i[4] is not ""):
							self.afficherErreur("Valeur actuelle n'est pas un nombre", "ERREUR")
							valide = False
							break

					if(i[4] is not ""): #Si la valeur est correct 
						valeurErrone = str(i[0])
						cur.execute(sql, ( float(i[4]), str(i[0]) ))

					else: #Si la valeur est vide
						self.afficherErreur("Valeur actuelle de '" + str(i[0]) + "' est vide", "ERREUR")
						valide = False
						break

				else: #Si la balance est un message
					sql = "UPDATE MESSAGE set CURRENTMESSAGE = :1 WHERE DESCRIPTION = :2"
					cur.execute(sql, (i[4], i[0]))
						
			if valide:
				self.afficherErreur("Operation effectuee avec succes", "SUCCES")

			cur.close()
			self.connection.commit()
		
		except cx_Oracle.DatabaseError as e:

			error,= e.args

			if valeurErrone is not "":
				self.afficherErreur(error.message + "La valeur de '" + valeurErrone + "' n'est pas dans les intervalles", "ERREUR")

			else:
				self.afficherErreur(error.message, "ERREUR")
