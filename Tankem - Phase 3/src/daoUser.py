# -*- coding: utf-8 -*-

from  	dtoUsers 		 		  import *
from	connectionOracleSingleton import *
import 	ctypes
import 	bcrypt

class DAOUser():

	def __init__(self):
		self.connection = None
		self.tempEmail  = ""
		self.dtoUsers   = DTOUsers

	def loginUser(self, numJoueur, email, password):

		cur 			=  ConnectionOracleSingleton().getConnectionOracle().cursor()
		sql 			= "SELECT * from USERSINFO WHERE EMAIL = :email"
		cur.execute(sql, {'email':email} )

		result = cur.fetchall() #Contient les informations de l'utilisateur pour l'authentification
		
		authentificationValide = False

		if(result):

			if self.tempEmail != email:

				if bcrypt.checkpw(password, result[0][2]):

					self.tempEmail 			= email
					authentificationValide 	= True

					sql = "SELECT * from ETAT_JOUEUR WHERE ID_USER IN (SELECT ID_USER FROM USERSINFO WHERE EMAIL = :email)"
					cur.execute(sql, {'email':email} )
					resultEtat = cur.fetchall() #Contient les informations de l'utilisateur pour le jeu
					resultEtat.append(self.hex_to_rgb(result[0][5])) #Ajoute la couleur convertie en rgb du tank du joueur
					resultEtat.append(result[0][1]) #Ajoute le nom d'utilisateur

					if(numJoueur == 1):			
						self.dtoUsers.joueur1 = resultEtat
					else:
						self.dtoUsers.joueur2 = resultEtat			
				else:
					ctypes.windll.user32.MessageBoxA(0, "Mot de passe incorrect", "Erreur", 0)
			else:
				ctypes.windll.user32.MessageBoxA(0, "Ce joueur est deja connecte", "Erreur", 0)
		else:
			ctypes.windll.user32.MessageBoxA(0, "Email inexistant", "Erreur", 0)

		cur.close()

		return authentificationValide

	def hex_to_rgb(self, value):
		value 	= value.lstrip('#')
		lv 		= len(value)

		return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))