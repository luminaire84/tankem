# -*- coding: utf-8 -*-

import csv

from daoConnectionOracle import *

daoConnectionOracle = DAOConnectionOracle()

nomFichier = "BalanceTankem.csv"
balance = []

def lireCSV():
	table = []
 	with open(nomFichier, 'rb') as file:
 		readerCSV = csv.reader(file, dialect='excel')
		readerCSV.next()
		for row in readerCSV:
			for cell in row:
				balance.append(cell.split(";"))

lireCSV()
daoConnectionOracle.ecrireBaseDonnee(balance)



	