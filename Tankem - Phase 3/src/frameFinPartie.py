# -*- coding: utf-8 -*-

#loadPrcFileData('', 'win-size 1280 720')

from 	pandac.PandaModules 			import loadPrcFileData
from 	direct.showbase.ShowBase 		import ShowBase
from 	direct.gui.OnscreenText 		import OnscreenText	
from 	direct.gui.DirectGui 			import *
from 	panda3d.core 					import TextNode 
from 	panda3d.core 					import Point3
from 	direct.interval.IntervalGlobal 	import *
from 	direct.interval.LerpInterval 	import *
import 	webbrowser
from 	daoStatistique 					import *
from 	analyseFinPartie 				import *
from    analyseDTOJoueur 				import *
from	connectionOracleSingleton 		import *

class FrameFinPartie(ShowBase):

	def __init__(self):
		self.analyseDTOJoueur   = AnalyseDTOJoueur()
		self.chrome_path  		= 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
		self.joueur1 	  		= None
		self.joueur2 	  		= None
		self.nomMap		  		= None
		self.idMap		  		= None
		self.dureePartie	    = None
		self.fontBlackOps 		= loader.loadFont('fonts/BlackOpsOne-Regular.ttf')	

		self.accept("joueurs", self.setJoueurs)
		self.accept("mapInfo", self.setMapInfo)

		self.daoStatistique    = DAOStatistique()
		self.analyseFinpartie  = AnalyseFinPartie()

		
	def setJoueurs(self, joueur1,  joueur2):

		self.joueur1 = joueur1
		self.joueur2 = joueur2

	def setMapInfo(self, nomMap, idMap, dureePartie):
		
		self.nomMap = nomMap
		self.idMap  = idMap
		self.dureePartie = dureePartie

	def gererResultatJoueur(self, idPerdant):	

		joueurFavoris = self.calcJoueurFavoris(self.joueur1.dtoJoueur[0], self.joueur2.dtoJoueur[0])
		
		# 1er cas : Le joueur 1 a perdu
		if self.joueur1.pointDeVie == 0:
			vieGagnantPerdue = self.joueur2.pointDeVieMax - self.joueur2.pointDeVie #Le perdant gagne de l'xp en fonction de la vie restant de son adversaire
			
			#Joueur1
			self.dtoPerdant = self.analyseFinpartie.expPerdant(vieGagnantPerdue, self.analyseDTOJoueur.nomCalcule(self.joueur1.dtoJoueur), self.joueur1.dtoJoueur[0], self.joueur1.nbArmeUtilisee)
			
			#On verifie si le joueur perdant est favoris pour donné un bonus au gagnant
			bonusFavori   = 0
			if joueurFavoris == self.joueur1.dtoJoueur[0][4]:
				bonusFavori = 1

			#Joueur2
			self.dtoGagnant  = self.analyseFinpartie.expGagnant(bonusFavori, self.joueur2.pointDeVie, self.analyseDTOJoueur.nomCalcule(self.joueur2.dtoJoueur), self.joueur2.dtoJoueur[0], self.joueur2.nbArmeUtilisee)
			
			self.finPartie(self.analyseDTOJoueur.nomCalcule(self.joueur2.dtoJoueur))
			self.finPartieGagnant(self.joueur2.dtoJoueur[2], self.dtoGagnant["bonusExp"], self.dtoGagnant["expTotal"], self.dtoGagnant["niveau"], self.dtoGagnant["seuilNextLevel"], self.dtoGagnant["lvlUp"])
			self.finPartiePerdant(self.joueur1.dtoJoueur[2], self.dtoPerdant["bonusExp"], self.dtoPerdant["expTotal"], self.dtoPerdant["niveau"], self.dtoPerdant["seuilNextLevel"], self.dtoPerdant["lvlUp"])

			#Pour l'animation de l'experience gagné
			self.valeurBonusGagnant = int(self.dtoGagnant["bonusExp"])
			self.valeurBonusPerdant = int(self.dtoPerdant["bonusExp"])	
			self.valeurExpGagnant   = int(self.joueur2.dtoJoueur[0][5])
			self.valeurExpPerdant   = int(self.joueur1.dtoJoueur[0][5])

			self.daoStatistique.insererPartie(self.joueur1.dtoJoueur[0][1], self.joueur2.dtoJoueur[0][1], self.idMap, self.dureePartie, 10, self.joueur2.dtoJoueur[0][1])
			self.daoStatistique.updateJoueurs(self.dtoGagnant, self.dtoPerdant)	
			self.daoStatistique.insererResultatsJoueurs(self.dtoGagnant, self.dtoPerdant, self.idMap)			
			
		# 2e cas : Le joueur 2 a perdu
		else:
			vieGagnantPerdue = self.joueur1.pointDeVieMax - self.joueur1.pointDeVie #Le perdant gagne de l'xp en fonction de la vie restant de son adversaire

			#Joueur2
			self.dtoPerdant = self.analyseFinpartie.expPerdant(vieGagnantPerdue, self.analyseDTOJoueur.nomCalcule(self.joueur2.dtoJoueur), self.joueur2.dtoJoueur[0], self.joueur2.nbArmeUtilisee)
			
			#On verifie si le joueur perdant est favoris pour donné un bonus au gagnant
			bonusFavori 	= 0
			if joueurFavoris == self.joueur2.dtoJoueur[0][4]:
				bonusFavori = 1

			#Joueur1
			self.dtoGagnant = self.analyseFinpartie.expGagnant(bonusFavori, self.joueur1.pointDeVie, self.analyseDTOJoueur.nomCalcule(self.joueur1.dtoJoueur), self.joueur1.dtoJoueur[0], self.joueur1.nbArmeUtilisee)

			self.finPartie(self.analyseDTOJoueur.nomCalcule(self.joueur1.dtoJoueur))
			self.finPartieGagnant(self.joueur1.dtoJoueur[2], self.dtoGagnant["bonusExp"], self.dtoGagnant["expTotal"], self.dtoGagnant["niveau"], self.dtoGagnant["seuilNextLevel"], self.dtoGagnant["lvlUp"])
			self.finPartiePerdant(self.joueur2.dtoJoueur[2], self.dtoPerdant["bonusExp"], self.dtoPerdant["expTotal"], self.dtoPerdant["niveau"], self.dtoPerdant["seuilNextLevel"], self.dtoPerdant["lvlUp"])

			#Pour l'animation de l'experience gagné
			self.valeurBonusGagnant = int(self.dtoGagnant["bonusExp"])
			self.valeurBonusPerdant = int(self.dtoPerdant["bonusExp"])
			self.valeurExpGagnant   = int(self.joueur1.dtoJoueur[0][5])
			self.valeurExpPerdant   = int(self.joueur2.dtoJoueur[0][5])

			self.daoStatistique.insererPartie(self.joueur1.dtoJoueur[0][1], self.joueur2.dtoJoueur[0][1], self.idMap, self.dureePartie, 10, self.joueur1.dtoJoueur[0][1])
			self.daoStatistique.updateJoueurs(self.dtoGagnant, self.dtoPerdant)	
			self.daoStatistique.insererResultatsJoueurs(self.dtoGagnant, self.dtoPerdant, self.idMap)						

		ConnectionOracleSingleton().disconnectOracle()

	def finPartie(self, nomJoueurCalcule):

		DirectFrame(image= "assets/tank_img.jpg", image_scale=(2,1,1))
		OnscreenText(text="FIN DE LA PARTIE", shadow=(0,0,0,0.9), pos=(0.0,0.80), scale=0.2, fg=(1,1,1,1), align=TextNode.ACenter, mayChange=0,font=self.fontBlackOps)
		OnscreenText(text="Niveau joué : "+self.nomMap, pos=(0.0,0.6), scale=0.09, fg=(0.6,0.1,0.5,1), align=TextNode.ACenter, mayChange=0,font=self.fontBlackOps)	
		OnscreenText(text=nomJoueurCalcule + " a gagné !", pos=(0.0,0.35), scale=0.1, fg=(0.9,0.9,0.1,1), align=TextNode.ACenter, mayChange=0,font=self.fontBlackOps)
		DirectButton(text= "Page joueur1", pos=(-0.8,0.0,-0.85), scale = 0.07, command=self.webPageA, pad=(1,0.45))
		DirectButton(text= "Page joueur2", pos=(0.8,0.0,-0.85), scale = 0.07, command=self.webPageB, pad=(1,0.45))


	def finPartieGagnant(self, nomJoueur, bonusExpWin, expTotal, niveau, seuilNextLevel, isLvlUp):

		OnscreenText(text=nomJoueur, pos=(0.8,0.17), shadow=(0,0,0,0.9), scale=0.11, fg=(0.9,0.9,0.1,1), align=TextNode.ACenter, mayChange=0)

		self.bonusGagnant 		= OnscreenText(text="Bonus d'expérience : +"+str(bonusExpWin), pos=(0.4,0), shadow=(0,0,0,0.8), scale=0.08, fg=(0,1,0,1), align=TextNode.ALeft, mayChange=0)	
		self.bonusAnimGagnant 	= Sequence(Func(self.bonusAnimationGagnant))
		self.bonusAnimGagnant.loop()

		self.expGagnant 		= OnscreenText(text="Expérience total : "+str(expTotal), pos=(0.4,-0.1), shadow=(0,0,0,0.8), scale=0.08, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)
		self.expAnimGagnant 	= Sequence(Func(self.expAnimationGagnant))
		self.expAnimGagnant.loop()

		self.niveau				= OnscreenText(text="Niveau : "+str(niveau), pos=(0.4,-0.2), shadow=(0,0,0,0.8), scale=0.07, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)
		self.seuil 				= OnscreenText(text="SeuilNiveauSuivant : "+str(seuilNextLevel), pos=(0.4,-0.3), shadow=(0,0,0,0.8), scale=0.07, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)

		if isLvlUp:
			lvlUp = OnscreenText(text = "Level up !", pos = (0.8,-0.6), scale = 0.2, fg=(1,1,0.5,1),align=TextNode.ACenter,mayChange=0)
			animLevelup = lvlUp.hprInterval(3, Point3(0.75,0.0,0), startHpr=Point3(-90,0))
			animLevelup.start()

	def finPartiePerdant(self, nomJoueur, bonusExpLose, expTotal, niveau, seuilNextLevel, isLvlUp):	

		OnscreenText(text=nomJoueur, pos=(-0.8,0.17), shadow=(0,0,0,0.8), scale=0.11, fg=(1,0,0,1), align=TextNode.ACenter, mayChange=0)

		self.bonusPerdant 		= OnscreenText(text="Bonus d'expérience : +"+str(bonusExpLose), pos=(-1.2,0), shadow=(0,0,0,0.8), scale=0.08, fg=(0,1,0,1), align=TextNode.ALeft, mayChange=0)
		self.bonusAnimPerdant 	= Sequence(Func(self.bonusAnimationPerdant))
		self.bonusAnimPerdant.loop()

		self.expPerdant 		= OnscreenText(text="Expérience total : "+str(expTotal), pos=(-1.2,-0.1), shadow=(0,0,0,0.8), scale=0.08, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)
		self.expAnimPerdant 	= Sequence(Func(self.expAnimationPerdant))
		self.expAnimPerdant.loop()

		self.niveau 			= OnscreenText(text="Niveau : "+str(niveau), pos=(-1.2,-0.2), shadow=(0,0,0,0.8), scale=0.07, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)
		self.seuil 				= OnscreenText(text="SeuilNiveauSuivant : "+str(seuilNextLevel), pos=(-1.2,-0.3), shadow=(0,0,0,0.8), scale=0.07, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)

		if isLvlUp:
			lvlUp = OnscreenText(text = "Level up !", pos = (-0.8,-0.6), scale = 0.2, fg=(1,1,0.5,1),align=TextNode.ACenter,mayChange=0)
			animLevelup = lvlUp.hprInterval(3, Point3(0.75,0.0,0), startHpr=Point3(-90,0))
			animLevelup.start()
	
	#-----------------------------------------------------------------------------------
	#------------------------------------ ANIMATION ------------------------------------
	
	def bonusAnimationGagnant(self):

		if self.valeurBonusGagnant <= 0:
			self.valeurBonusGagnant = 0			
			self.bonusAnimGagnant.finish()		

		else:
			self.valeurBonusGagnant -= 3	

		self.bonusGagnant.destroy()
		self.bonusGagnant = OnscreenText(text="Bonus d'expérience : +" + str(self.valeurBonusGagnant), pos=(0.4,0), shadow=(0,0,0,0.8), scale=0.08, fg=(0,1,0,1), align=TextNode.ALeft, mayChange=0)

	def bonusAnimationPerdant(self):

		if self.valeurBonusPerdant <= 0:
			self.valeurBonusPerdant = 0		
			self.bonusAnimPerdant.finish()

		else:
			self.valeurBonusPerdant -= 3

		self.bonusPerdant.destroy()
		self.bonusPerdant = OnscreenText(text="Bonus d'expérience : +" + str(self.valeurBonusPerdant), pos=(-1.2,0), shadow=(0,0,0,0.8), scale=0.08, fg=(0,1,0,1), align=TextNode.ALeft, mayChange=0)

	def expAnimationGagnant(self):

		if self.valeurExpGagnant >= int(self.dtoGagnant["expTotal"]):
			self.valeurExpGagnant = int(self.dtoGagnant["expTotal"])				
			self.expAnimGagnant.finish()

		else:
			self.valeurExpGagnant += 3	

		self.expGagnant.destroy()
		self.expGagnant = OnscreenText(text="Expérience total : " + str(self.valeurExpGagnant), pos=(0.4,-0.1), shadow=(0,0,0,0.8), scale=0.08, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)

			

	def expAnimationPerdant(self):

		if self.valeurExpPerdant >= int(self.dtoPerdant["expTotal"]):
			self.valeurExpPerdant = int(self.dtoPerdant["expTotal"])		
			self.expAnimPerdant.finish()

		else:
			self.valeurExpPerdant += 3

		self.expPerdant.destroy()
		self.expPerdant = OnscreenText(text="Expérience total : " + str(self.valeurExpPerdant), pos=(-1.2,-0.1), shadow=(0,0,0,0.8), scale=0.08, fg=(1,1,1,1), align=TextNode.ALeft, mayChange=0)


	def webPageA(self):

		self.url = "http://localhost/Web/login.php"
		webbrowser.get(self.chrome_path).open(self.url)

	def webPageB(self):

		self.url = "http://localhost/Web/login.php"
		webbrowser.get(self.chrome_path).open(self.url)

	def calcJoueurFavoris(self, joueur1, joueur2):

		joueurFavorise = ""

		if joueur1[2] > joueur2[2]:
			joueurFavorise = joueur1[4]

		if joueur2[2] > joueur1[2]:	
			joueurFavorise = joueur2[4]
		
		return joueurFavorise 