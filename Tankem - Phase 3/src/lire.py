# -*- coding: utf-8 -*-

import csv
import codecs
from Tkinter import Tk
from tkFileDialog import asksaveasfilename
import cx_Oracle
import subprocess
import os
import ctypes

from daoConnectionOracle import *

daoConnectionOracle = DAOConnectionOracle()

#--------------------------------- Lire un fichier CSV
def creerFichierCSV():
	Tk().withdraw();

	filename = asksaveasfilename( filetypes=[("CSV Files","*.csv"),("All Files","*.*")], defaultextension = "*.csv", initialfile="BalanceTankem");

	return filename;

def viderCSV():	#vider les données dans le fichier(remplacer tout par le vide)
	with open(nomFichier, 'w') as file:     
		writerCSV = csv.writer(file,dialect='excel')


def ecrireTitres():
	with open(nomFichier, 'ab') as file:	#écrire les données dans le fichier
		writerCSV = csv.writer(file,dialect='excel', delimiter=';')
		writerCSV.writerow(("Description balance", "Valeur Min", "Valeur Max", "Valeur Defaut", "Valeur Choisie"))

def ecrireCSV(donnees):
	with open(nomFichier, 'ab') as file:	#écrire les données dans le fichier
		writerCSV = csv.writer(file,dialect='excel', delimiter=';')

		for i in donnees:
			writerCSV.writerow((i[1], i[2], i[3], i[4], i[5]))


nomFichier = creerFichierCSV() #Creer le csv

viderCSV() #Vide le csv s'il y a du contenu

fn = os.path.join(os.path.dirname(__file__), nomFichier) #Prendre le csv pour l'ouvrir automatiquement

ecrireTitres()

ecrireCSV(daoConnectionOracle.resultStats)
ecrireCSV(daoConnectionOracle.resultMessages)

subprocess.Popen([fn], shell=True)


