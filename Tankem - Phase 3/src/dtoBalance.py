# -*- coding: utf-8 -*-

class DTOBalance():
    def __init__(self):
        self.balanceMin      = {}
        self.balanceMax      = {}
        self.balanceDefaut   = {}
        self.balanceActuelle = {}

        #Balance DTO Valeurs par défauts --------------------------------
        #Balance Tanks
        self.balanceActuelle["Vitesse des chars"]                 = 7
        self.balanceActuelle["Vitesse de rotation des chars"]     = 1500;
        self.balanceActuelle["Points de vie des chars"]           = 200;
        self.balanceActuelle["Canon - temps de recharge"]         = 1.2;
        self.balanceActuelle["Mitraillette - temps de recharge"]  = 0.4;
        self.balanceActuelle["Grenade - temps de recharge"]       = 0.8;
        self.balanceActuelle["Shotgun - temps de recharge"]       = 1.8;
        self.balanceActuelle["Shotgun - ouverture du fusil"]      = 0.4;
        self.balanceActuelle["Piege - temps de recharge"]         = 0.8;
        self.balanceActuelle["Missile guide - temps de recharge"] = 3.0;
        self.balanceActuelle["Spring - temps de recharge"]        = 0.5;
        self.balanceActuelle["Spring - vitesse initiale du saut"] = 10;

        #Balance Balles
        self.balanceActuelle["Canon - vitesse balle"]                = 14
        self.balanceActuelle["Mitraillette - vitesse balle"]         = 18
        self.balanceActuelle["Grenade - vitesse initiale balle"]     = 16 
        self.balanceActuelle["Shotgun - vitesse balle"]              = 13
        self.balanceActuelle["Piege - vitesse balle"]                = 1
        self.balanceActuelle["Missile guide - vitesse guidee balle"] = 30
        self.balanceActuelle["Grosseur explosion des balles"]        = 8