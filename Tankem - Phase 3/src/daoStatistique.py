# -*- coding: utf-8 -*-

import 	cx_Oracle
import 	time
from connectionOracleSingleton import *

class DAOStatistique():

	def __init__(self):
		self.connection = None

	def insererPartie(self, IdUser1, IdUser2, idMap, duree, resolution, idVainqueur):

		try:
			self.connection = ConnectionOracleSingleton().getConnectionOracle()
			curEcrire = self.connection.cursor()

			statement = 'INSERT INTO PARTIES(ID_USER1, ID_USER2, ID_MAP, DUREE_PARTIE, RESOLUTIONS, ID_VAINQUEUR) values (:1, :2, :3, :4, :5, :6)'
			curEcrire.execute(statement, (IdUser1, IdUser2, idMap, duree, resolution, idVainqueur))
			
			curEcrire.close()
			self.connection.commit()

			cur = self.connection.cursor()
			statement ='SELECT max(id_partie) FROM parties' #On récupère la dernière l'id de la partie (la dernière)
			cur.execute(statement)
			resultat = cur.fetchall()
			self.idPartie = resultat[0][0]
		
		except cx_Oracle.DatabaseError as e:
			print e
			print("Problème d'écriture de la base de données")

	def updateJoueurs(self, dtoJoueur1, dtoJoueur2):
		try:
			self.connection = ConnectionOracleSingleton().getConnectionOracle()
			cur = self.connection.cursor()

			print dtoJoueur1
			statement = 'UPDATE ETAT_JOUEUR SET NIVEAU = :1, SEUIL_NIVEAU_SUIVANT = :2, NOM_CALCULE = :3, EXPERIENCE_TOTAL = :4, POINTS_STATS = :5 WHERE ID_USER = :6'
			cur.execute(statement, (dtoJoueur1["niveau"], dtoJoueur1["seuilNextLevel"], dtoJoueur1["nomCalcule"], dtoJoueur1["expTotal"], dtoJoueur1["pointsStats"], dtoJoueur1["idUser"]))

			statement = 'UPDATE ETAT_JOUEUR SET NIVEAU = :1, SEUIL_NIVEAU_SUIVANT = :2, NOM_CALCULE = :3, EXPERIENCE_TOTAL = :4, POINTS_STATS = :5 WHERE ID_USER = :6'
			cur.execute(statement, (dtoJoueur2["niveau"], dtoJoueur2["seuilNextLevel"], dtoJoueur2["nomCalcule"], dtoJoueur2["expTotal"], dtoJoueur2["pointsStats"], dtoJoueur2["idUser"]))
			
			cur.close()
			self.connection.commit()

		except cx_Oracle.DatabaseError as e:
			print e
			print("Problème d'écriture de la base de données")

	def insererResultatsJoueurs(self, dtoJoueur1, dtoJoueur2, idMap):
		try:
			self.connection = ConnectionOracleSingleton().getConnectionOracle()
			cur = self.connection.cursor()

			statement = 'INSERT INTO RESULTAT_PARTIE_JOUEUR(ID_USER, ID_PARTIE, ID_MAP, BALLE, SHOTGUN, GRENADE, PIEGE, GUIDE, SPRING, MITRAILLETTE, RESULTAT_PARTIE) values (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11)'
			cur.execute(statement, (dtoJoueur1["idUser"], self.idPartie, idMap, dtoJoueur1["nbArmesUtilisees"]["Canon"], dtoJoueur1["nbArmesUtilisees"]["Shotgun"], dtoJoueur1["nbArmesUtilisees"]["Grenade"],
									dtoJoueur1["nbArmesUtilisees"]["Piege"], dtoJoueur1["nbArmesUtilisees"]["Guide"], dtoJoueur1["nbArmesUtilisees"]["Spring"], dtoJoueur1["nbArmesUtilisees"]["Mitraillette"], dtoJoueur1["resultat"]))

			statement = 'INSERT INTO RESULTAT_PARTIE_JOUEUR(ID_USER, ID_PARTIE, ID_MAP, BALLE, SHOTGUN, GRENADE, PIEGE, GUIDE, SPRING, MITRAILLETTE, RESULTAT_PARTIE) values (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11)'
			cur.execute(statement, (dtoJoueur2["idUser"], self.idPartie, idMap, dtoJoueur2["nbArmesUtilisees"]["Canon"], dtoJoueur2["nbArmesUtilisees"]["Shotgun"], dtoJoueur2["nbArmesUtilisees"]["Grenade"],
									dtoJoueur2["nbArmesUtilisees"]["Piege"], dtoJoueur2["nbArmesUtilisees"]["Guide"], dtoJoueur2["nbArmesUtilisees"]["Spring"], dtoJoueur2["nbArmesUtilisees"]["Mitraillette"], dtoJoueur2["resultat"]))

			cur.close()
			self.connection.commit()

		except cx_Oracle.DatabaseError as e:
			print e
			print("Problème d'écriture de la base de données")