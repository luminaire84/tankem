--set echo off
--PREMI�RE TABLE
CREATE TABLE INFO_JEU(
    ID NUMBER GENERATED ALWAYS AS IDENTITY,
    description VARCHAR2(60) NOT NULL,
    valeurMin NUMBER(5,1),
    maximum NUMBER(5,1),
    defaut  NUMBER(5,1),
    currentValue NUMBER(5,1),
    PRIMARY KEY (ID),
    CONSTRAINT check_min CHECK(currentValue >= valeurMin),
    CONSTRAINT check_max CHECK(currentValue <= maximum)
);

-- Insertion de valeurs dans la table
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Vitesse des chars',4.0,12.0,7.0,7.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Vitesse de rotation des chars',1000.0,2000.0,1500.0,1500.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Points de vie des chars',100.0,2000.0,200.0,200.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('temps du mouvement des blocs anim�s',0.2,2.0,0.8,0.8);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Canon - vitesse de balle',4.0,30.0,14.0,14.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Canon - temps de recharge',0.2,10.0,1.2,1.2);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Mitraillette - vitesse de balle',4.0,30.0,18.0,18.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Mitaillette - temps de recharge',0.2,10.0,0.4,0.4);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Grenade - vitesse initiale balle',10.0,25.0,16.0,16.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Grenade - temps de recharge',0.2,10.0,0.8,0.8);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('shotgun - vitesse balle',4.0,30.0,13.0,13.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('shotgun - temps de recharge',0.2,10.0,1.8,1.8);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('shotgun - ouverture du fusil',0.1,1.5,0.4,0.4);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Pi�ge - vitesse balle',0.2,4.0,1.0,1.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Pi�ge - temps de recharge',0.2,10.0,0.8,0.8);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Missile guid� - vitesse guid�e balle',20.0,40.0,30.0,30.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Missile guid� - temps de recharge',0.2,10.0,3.0,3.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Spring - vitesse initiale du saut',6.0,20.0,10.0,10.0);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Spring - temps de recharge',0.2,10.0,0.5,0.5);
INSERT INTO INFO_JEU (description,valeurMin,maximum,defaut, currentValue) VALUES ('Grosseur explosion des balles',1.0,30.0,8.0,8.0);
COMMIT;

