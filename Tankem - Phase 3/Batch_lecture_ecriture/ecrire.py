# -*- coding: utf-8 -*-

import csv
import cx_Oracle
import ctypes

nomFichier = "BalanceTankem.csv"
balance = []

def lireCSV():
	table = []
 	with open(nomFichier, 'rb') as file:
 		readerCSV = csv.reader(file, dialect='excel')
		readerCSV.next()
		for row in readerCSV:
			for cell in row:
				balance.append(cell.split(";"))

def afficherErreur(message, titre):
	ctypes.windll.user32.MessageBoxA(0, str(message), titre, 0)

def ecrireBaseDonnee():
	valide = True
	valeurErrone = ""
	try:
		connexion = cx_Oracle.connect('e1464900','B','10.57.4.60/DECINFO.edu')
		cur = connexion.cursor()

		for i in balance:
			if "Message" not in i[0]: #Si la balance est un stats
				sql = "UPDATE INFO_JEU set CURRENTVALUE = :1 WHERE DESCRIPTION = :2"

				#Si la valeur n'est pas un nombre et n'est pas vide
				try:
					if(float(i[4]) is str and i[4] is not ""):
						afficherErreur("Valeur actuelle n'est pas un nombre", "ERREUR")
						valide = False
						break
				except ValueError:
					if(i[4] is not ""):
						afficherErreur("Valeur actuelle n'est pas un nombre", "ERREUR")
						valide = False
						break

				if(i[4] is not ""): #Si la valeur est correct 
					valeurErrone = str(i[0])
					cur.execute(sql, ( float(i[4]), str(i[0]) ))

				else: #Si la valeur est vide
					afficherErreur("Valeur actuelle de '" + str(i[0]) + "' est vide", "ERREUR")
					valide = False
					break

			else: #Si la balance est un message
				sql = "UPDATE MESSAGE set CURRENTMESSAGE = :1 WHERE DESCRIPTION = :2"
				cur.execute(sql, (i[4], i[0]))
					
		if valide:
			afficherErreur("Operation effectuee avec succes", "SUCCES")

		cur.close()
		connexion.commit()
		connexion.close()
	except cx_Oracle.DatabaseError as e:
		error,= e.args
		if valeurErrone is not "":
			afficherErreur(error.message + "La valeur de '" + valeurErrone + "' n'est pas dans les intervalles", "ERREUR")
		else:
			afficherErreur(error.message, "ERREUR")
		
lireCSV()
ecrireBaseDonnee()


	