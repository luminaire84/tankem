# -*- coding: utf-8 -*-

class Sanitizer():
    def __init__(self):
        pass

    def cleanDTO(self, DTOBalance):
		if(DTOBalance.balanceActuelle): #Si le DTO contient les valeurs actuelles de la balance
		
			for key in DTOBalance.balanceActuelle:
				if(key == "Message - signal début de partie - contenu" and key == "Message d'acceuil - contenu"):
					pass

				#Si inférieur au minimum ou supérieur au max, affecte la valeur par défaut
				elif(DTOBalance.balanceActuelle[key] < DTOBalance.balanceMin[key] or DTOBalance.balanceActuelle[key] > DTOBalance.balanceMax[key]): 
					DTOBalance.balanceActuelle[key] = DTOBalance.balanceDefaut[key]
