# -*- coding: utf-8 -*-

from balanceDTO import *

import cx_Oracle

class BalanceDAO():
	def __init__(self):
		self.resultStats    = None
		self.resultMessages = None
		self.LireBD()

	def LireBD(self):
		try:
			con = cx_Oracle.connect('e1464900', "B",'10.57.4.60/DECINFO.edu')

			curRead = con.cursor()

			curRead.execute("select * from INFO_JEU")
			self.resultStats = curRead.fetchall() #Creer un tableau à partir du curseur

			curRead.execute("select * from MESSAGE")
			self.resultMessages = curRead.fetchall() #Creer un tableau à partir du curseur

			curRead.close()
			con.close()

		except cx_Oracle.DatabaseError as e:
			print("Problème de lecture de la base de données")


	def construireDTO(self):
		balanceDTO = BalanceDTO()

		if(self.resultStats is not None and self.resultMessages is not None):
			for i in self.resultStats:
				print i

			for i in self.resultStats:
				balanceDTO.balanceMin[i[1]] 	  = i[2]
				balanceDTO.balanceMax[i[1]] 	  = i[3]
				balanceDTO.balanceDefaut[i[1]]    = i[4]
				balanceDTO.balanceActuelle[i[1]]  = i[5]

			for i in self.resultMessages:
				balanceDTO.balanceMin[i[1]] 	  = i[2]
				balanceDTO.balanceMax[i[1]] 	  = i[3]
				balanceDTO.balanceDefaut[i[1]]    = i[4]

		return balanceDTO