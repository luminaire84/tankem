--cr�ation de la deuxi�me table
CREATE TABLE MESSAGE(
    ID NUMBER GENERATED ALWAYS AS IDENTITY,
    description VARCHAR2(60) NOT NULL,
    valeurMin VARCHAR2(60),
    maximum VARCHAR2(70),
    defaut  VARCHAR2(60),
	currentMesssage VARCHAR2(70),
    PRIMARY KEY (ID)
    -- CONSTRAINT check_message CHECK( maximum < 71.0)
);

-- Insertion de valeurs dans la table
INSERT INTO MESSAGE (description,valeurMin,maximum,defaut,currentMesssage) VALUES ('Message accueil - contenu',0.0,60.0,'Tankem!','Tankem!');
INSERT INTO MESSAGE (description,valeurMin,maximum,defaut,currentMesssage) VALUES ('Message accueil - dur�e',1.0,10.0,3.0,3.0);
INSERT INTO MESSAGE (description,valeurMin,maximum,defaut,currentMesssage) VALUES ('Message compte � rebours - dur�e',0.0,10.0,3.0,3.0);
INSERT INTO MESSAGE (description,valeurMin,maximum,defaut,currentMesssage) VALUES ('Message - signal d�but de partie - contenu',0.0,50.0,'Debut de partie','Debut de partie');
INSERT INTO MESSAGE (description,valeurMin,maximum,defaut,currentMesssage) VALUES ('Message fin de partie - contenu',0.0,70.0,'Game over','Game over');
COMMIT;